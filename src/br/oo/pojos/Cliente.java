package br.oo.pojos;

public class Cliente extends Pessoa {
	private Conta conta;

	public  void sacar(double quantidade) {
		this.conta.setSaldo(getConta().getSaldo() - quantidade);
	}

	public void depositar(double quantidade) {
		this.conta.setSaldo(getConta().getSaldo() + quantidade);
	}

	public double verSaldo() {
		return conta.getSaldo();
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

}
