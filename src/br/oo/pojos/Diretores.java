package br.oo.pojos;

import br.oo.controllers.Banco;

public class Diretores extends Gerente{
	private int nSetores;

	//Pesquisa dados do funcionario
	private Funcionario pesquisaFuncionarios(String nome, Banco banco){
		for (Pessoa p : banco.getPessoas()) {
			if (p.getNome() == nome) {
				return (Funcionario) p;
			}
		}
			return null;
	}

	public int getnSetores() {
		return nSetores;
	}

	public void setnSetores(int nSetores) {
		this.nSetores = nSetores;
	}

}
