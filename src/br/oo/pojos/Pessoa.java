package br.oo.pojos;

public class Pessoa{
	protected String nome;
	//F para fisica e J para juridica
	protected char tipo;
	protected String comprovantePessoa;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	
	
}
