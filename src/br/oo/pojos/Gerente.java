package br.oo.pojos;

import br.oo.controllers.Banco;

public class Gerente extends Funcionario {
	private String senha;
	private int nFuncionarios;
	
	//Pesquisa sobre algum cliente
	private Cliente pesquisarCliente(String nome, Banco banco){
		for (Pessoa p : banco.getPessoas()) {
			if (p.getNome() == nome) {
				return (Cliente) p;
			}
		}
			return null;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getnFuncionarios() {
		return nFuncionarios;
	}

	public void setnFuncionarios(int nFuncionarios) {
		this.nFuncionarios = nFuncionarios;
	}

}
