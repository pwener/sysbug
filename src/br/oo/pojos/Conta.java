package br.oo.pojos;

public class Conta {
	private String numero;
	// C para corrent P para poupança
	private char tipoConta;
	private double saldo;
	
	public Conta() {
		
	}
	
	public Conta(String numero, char tipoConta, double saldo) {
		this.numero = numero;
		this.tipoConta = tipoConta;
		this.saldo = saldo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public char getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(char tipoConta) {
		this.tipoConta = tipoConta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

}
