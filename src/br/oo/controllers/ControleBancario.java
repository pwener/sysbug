package br.oo.controllers;

import br.oo.pojos.Conta;

public class ControleBancario {
	private static Banco banco;
	
	public ControleBancario(Banco banco) {
		this.banco = banco;
	}
	
	// Apenas simulação...
	public void adiciona(Conta c) {
		banco.getContas().add(c);
	}
	
	//Itera no pegaConta e pega a conta de numero x
	public Conta pegaConta(String x) {
		for (Conta c : banco.getContas()) {
			if (c.getNumero() == x) {
				return c;
			}
		}
		return null;
	}
	
	public int pegaTotalDeContas() {
		return banco.getContas().size();
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
}
