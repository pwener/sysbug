package br.oo.controllers;

import java.util.ArrayList;

import br.oo.pojos.Conta;
import br.oo.pojos.Pessoa;

public class Banco {
	private ArrayList<Pessoa> pessoas;
	private ArrayList<Conta> contas;
	
	public Banco(){
		this.contas = new ArrayList<Conta>();
		this.pessoas = new ArrayList<Pessoa>();
	}

	public ArrayList<Pessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(ArrayList<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

	public ArrayList<Conta> getContas() {
		return contas;
	}

	public void setContas(ArrayList<Conta> contas) {
		this.contas = contas;
	}

}
