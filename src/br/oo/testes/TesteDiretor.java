package br.oo.testes;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.oo.controllers.Banco;
import br.oo.pojos.Cliente;
import br.oo.pojos.Diretor;
import br.oo.pojos.Funcionario;

public class TesteDiretor {
	private Funcionario funcionario;
	private Diretor diretor;
	private Banco banco;

	@Before
	public void setup() {
		this.funcionario = new Funcionario();
		funcionario.setNome("Phelipe");
		funcionario.setTipo('F');
		this.banco = new Banco();
		banco.getPessoas().add(funcionario);
		this.diretor = new Diretor();
	}

	@Test
	public void testaPesquisaFuncionarios(){
		Funcionario f = diretor.pesquisaFuncionarios("Phelipe", this.banco);
		assertEquals("Phelipe", f.getNome());
	}
}
